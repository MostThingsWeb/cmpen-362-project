// Graphical portions of this project are based off of Qt graphics network example (originally copyright below).
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "link.h"
#include "utility.h"

#include <math.h>
#include <random>

#include <QPen>
#include <QPainter>
#include <QDebug>

Link::Link(Router* startItem, Router* endItem, QGraphicsItem* parent) : QGraphicsLineItem(parent) {
    _capacityKbps = Utility::randNormal(128, 32);
    _source = startItem;
    _dest = endItem;
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    myColor = Qt::black;
    setPen(QPen(myColor, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

    label = new LinkLabel(QString::number(_capacityKbps), this);

    QObject::connect(label, &LinkLabel::linkCapacityChanged, [this](int _capacityKbps){
        this->_capacityKbps = _capacityKbps;
    });
}

int Link::type() const {
    return Type;
}

QPainterPath Link::shape() const {
    QGraphicsLineItem copy;
    copy.setLine(line().x1(), line().y1(), line().x2(), line().y2());
    copy.setPen(QPen(myColor, 20));
    return copy.shape();
}

void Link::setColor(const QColor &color) {
    myColor = color;
}

Router *Link::source() const {
    return _source;
}

Router *Link::dest() const {
    return _dest;
}

int Link::capacityKbps() const {
    return _capacityKbps;
}

void Link::updatePosition() {
    QLineF line(mapFromItem(_source, 0, 0), mapFromItem(_dest, 0, 0));
    setLine(line);
    label->updatePosition();
}

void Link::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*) {
    if (_source->collidesWithItem(_dest)) {
        return;
    }

    QPen myPen = pen();
    myPen.setColor(myColor);
    painter->setPen(myPen);
    painter->setBrush(myColor);

    setLine(QLineF(_source->pos(), _dest->pos()));
    painter->drawLine(line());

    if (isSelected()) {
        painter->setPen(QPen(myColor, 1, Qt::DashLine));
        QLineF dashedLine = line();

        // Draw a dashed line on both sides of the link (translated in direction of normal vector)
        QLineF normal = dashedLine.normalVector();
        normal.setLength(4);
        dashedLine.translate(normal.dx(), normal.dy());
        painter->drawLine(dashedLine);
        dashedLine.translate(-2 * normal.dx(), -2 * normal.dy());
        painter->drawLine(dashedLine);
    }
}
