// Graphical portions of this project are based off of Qt graphics scene example (originally copyright below).
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "network.h"
#include "link.h"

#include <cassert>
#include <iostream>

#include <QTextCursor>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QStack>

Network::Network(QMenu* itemMenu, QObject* parent) : QGraphicsScene(parent) {
    myItemMenu = itemMenu;
    line = 0;
}

QList<Router*> Network::routers() {
    // Return the set of routers
    QList<Router*> ret;
    for (QGraphicsItem* item : items()) {
        Router* asRouter = item ? dynamic_cast<Router*>(item) : nullptr;
        if (asRouter) {
            ret.append(asRouter);
        }
    }

    return ret;
}

QList<QString> Network::routerLabels() {
    QList<QString> labels;
    for (Router* router : routers()) {
        labels.append(router->label());
    }

    std::sort(labels.begin(), labels.end());
    return labels;
}

Network::FullRoutingTable Network::generateAllOptimalPaths() {
    // Simulate link-state routing
    // Run Dijkstra on every router to generate its routing table.
    // Goal is to minimize number of hops

    QList<Router*> allRouters = routers();
    // map (source, dest) => [progression of links to take]
    FullRoutingTable fullTable;

    // Run Dijkstra for each router
    for (Router* source : allRouters) {
        QMap<Router*, Router*> table = routeNode(source);

        // Find optimal path between source and all other routers
        for (Router* dest : allRouters) {
            if (source == dest) {
                continue;
            }

            QStack<Router*> path;
            Router* u = dest;
            while (table.contains(u)) {
                path.push(u);
                u = table[u];
            }
            path.push(u);

            // Record optimal path
            while (!path.empty()) {
                fullTable[QPair<Router*, Router*>(source, dest)].append(path.pop());
            }
        }

    }

    return fullTable;
}

QMap<Router*, Router*> Network::routeNode(Router *source) {
    QMap<Router*, Router*> table;
    if (source->links().empty()) {
        return table;
    }

    QList<Router*> allRouters = routers();
    qDebug() << "+ router " << source->label();

    // Initialize list of unvisited routers
    QList<Router*> unvisited = allRouters;
    QMap<Router*, int> distances;
    distances[source] = 0;
    for (Router* r : allRouters) {
        if (source != r) {
            distances[r] = INT_MAX;
        }
    }

    while (!unvisited.empty()) {
        // Find the next router to visit (that with smallest # of hops)
        Router* u = nullptr;
        int leastHops = INT_MAX;
        for (Router* candidate : unvisited) {
            if (distances[candidate] <= leastHops) {
                u = candidate;
                leastHops = distances[candidate];
            }
        }

        // Remove router from unvisited set
        unvisited.removeOne(u);

        // Process its neighbors
        for (Router* v : u->neighbors()) {
            int newDistance =  distances[u] + 1;
            if (newDistance < distances[v]) {
                distances[v] = newDistance;
                table[v] = u;
            }
        }
    }

    return table;
}

void Network::setMode(Mode mode) {
    myMode = mode;
}

void Network::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) {
    if (mouseEvent->button() != Qt::LeftButton) {
        return;
    }

    mousePressPoint = mouseEvent->scenePos();

    // If the user clicks a router in AddRouter mode, then change to CreateLink mode.
    // If the user releases the mouse in the same exact spot, mode will be changed to MoveItem.
    if (itemAt(mouseEvent->scenePos(), QTransform()) && myMode == Mode::AddRouter) {
        myMode = Mode::CreateLink;
        wasModeAddRouter = true;
        emit forceModeChange(myMode);
    }

    Router* item;
    switch (myMode) {
    case Mode::AddRouter:
        item = new Router(myItemMenu);
        item->setBrush(Qt::white);
        addItem(item);
        item->setPos(mouseEvent->scenePos());
        item->computeLabel();
        emit itemInserted(item);
        return;
    case Mode::CreateLink:
        line = new QGraphicsLineItem(QLineF(mouseEvent->scenePos(), mouseEvent->scenePos()));
        line->setPen(QPen(Qt::black, 2));
        addItem(line);
        break;
    default:
        break;
    }

    QGraphicsScene::mousePressEvent(mouseEvent);
}

void Network::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent) {
    if (myMode == Mode::CreateLink && line != 0) {
        QLineF newLine(line->line().p1(), mouseEvent->scenePos());
        line->setLine(newLine);
    } else if (myMode == Mode::MoveItem) {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void Network::mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent) {
    // If mouse is just clicked (i.e. press position == release position), then change to move mode
    if (wasModeAddRouter && myMode == Mode::CreateLink && mouseEvent->scenePos() == mousePressPoint) {
        myMode = Mode::MoveItem;
        wasModeAddRouter = false;

        if (line) {
            removeItem(line);
            delete line;
        }

        emit forceModeChange(myMode);
    }

    if (line && myMode == Mode::CreateLink) {
        QList<QGraphicsItem*> startItems = items(line->line().p1());
        if (startItems.count() && startItems.first() == line) {
            startItems.removeFirst();
        }
        QList<QGraphicsItem*> endItems = items(line->line().p2());
        if (endItems.count() && endItems.first() == line) {
            endItems.removeFirst();
        }

        removeItem(line);
        delete line;

        if (startItems.count() > 0 && endItems.count() > 0 &&
                startItems.first()->type() == Router::Type &&
                endItems.first()->type() == Router::Type &&
                startItems.first() != endItems.first()) {
            Router* startItem = qgraphicsitem_cast<Router*>(startItems.first());
            Router* endItem = qgraphicsitem_cast<Router*>(endItems.first());

            // Only allow one item between any given pair of items
            if (!startItem->linkTo(endItem)) {
                Link* link = new Link(startItem, endItem);
                link->setColor(Qt::black);
                startItem->addlink(link);
                endItem->addlink(link);
                link->setZValue(-1000.0);
                addItem(link);
                link->updatePosition();
            }
        }

        if (wasModeAddRouter) {
            myMode = Mode::AddRouter;
            wasModeAddRouter = false;
            emit forceModeChange(myMode);
        }
    }

    line = 0;
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}
