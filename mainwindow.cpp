// Graphical portions of this project are based off of Qt graphics scene example (originally copyright below).
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "router.h"
#include "network.h"
#include "link.h"
#include "mainwindow.h"

#include <QGraphicsView>
#include <QButtonGroup>
#include <QMessageBox>
#include <QLabel>
#include <QSplitter>
#include <QMenuBar>
#include <QToolBar>
#include <QDebug>


MainWindow::MainWindow() {
    createActions();
    createMenus();

    network = new Network(itemMenu, this);
    network->setSceneRect(QRectF(0, 0, 5000, 5000));

    connect(network, &Network::itemInserted, this, &MainWindow::itemInserted);
    connect(network, &Network::selectionChanged, [this](){
        auto items = network->selectedItems();

        if (items.empty()) {
            leftPanel->trafficMatrix()->clearSelection();
        } else {
            for (auto item : items) {
                this->itemsSelected(item);
            }
        }
    });

    createToolbars();
    network->setMode(static_cast<Network::Mode>(pointerTypeGroup->checkedId()));
    view = new QGraphicsView(network);

    leftPanel = new LeftPanel();

    QSplitter* splitter = new QSplitter();
    splitter->addWidget(leftPanel);
    splitter->addWidget(view);

    connect(leftPanel->analyzeButton(), &QPushButton::clicked, this, &MainWindow::analyzeNetwork);

    setCentralWidget(splitter);
    setWindowTitle(tr("CMPEN 362 network analyzer - Chris Laplante"));
    setUnifiedTitleAndToolBarOnMac(true);
}

void MainWindow::deleteItem() {
    foreach(QGraphicsItem * item, network->selectedItems()) {
        if (item->type() == Link::Type) {
            network->removeItem(item);
            Link* link = qgraphicsitem_cast<Link*>(item);
            link->source()->removelink(link);
            link->dest()->removelink(link);
            delete item;
        }
    }

    foreach(QGraphicsItem * item, network->selectedItems()) {
        if (item->type() == Router::Type) {
            qgraphicsitem_cast<Router*>(item)->removelinks();
        }
        network->removeItem(item);
        delete item;
    }

    leftPanel->trafficMatrix()->rebuild(network->routerLabels());
}

void MainWindow::pointerGroupClicked(int) {
    network->setMode(static_cast<Network::Mode>(pointerTypeGroup->checkedId()));
}

void MainWindow::bringToFront() {
    if (network->selectedItems().isEmpty()) {
        return;
    }

    QGraphicsItem* selectedItem = network->selectedItems().first();
    QList<QGraphicsItem*> overlapItems = selectedItem->collidingItems();

    qreal zValue = 0;
    foreach(QGraphicsItem * item, overlapItems) {
        if (item->zValue() >= zValue && item->type() == Router::Type) {
            zValue = item->zValue() + 0.1;
        }
    }
    selectedItem->setZValue(zValue);
}

void MainWindow::sendToBack() {
    if (network->selectedItems().isEmpty()) {
        return;
    }

    QGraphicsItem* selectedItem = network->selectedItems().first();
    QList<QGraphicsItem*> overlapItems = selectedItem->collidingItems();

    qreal zValue = 0;
    foreach(QGraphicsItem * item, overlapItems) {
        if (item->zValue() <= zValue && item->type() == Router::Type) {
            zValue = item->zValue() - 0.1;
        }
    }
    selectedItem->setZValue(zValue);
}

void MainWindow::itemInserted(Router*) {
    leftPanel->trafficMatrix()->rebuild(network->routerLabels());
}

void MainWindow::sceneScaleChanged(const QString& scale) {
    double newScale = scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
    QMatrix oldMatrix = view->matrix();
    view->resetMatrix();
    view->translate(oldMatrix.dx(), oldMatrix.dy());
    view->scale(newScale, newScale);
}

void MainWindow::itemsSelected(QGraphicsItem* item) {
    Link* asLink = dynamic_cast<Link*>(item);
    if (asLink) {
        leftPanel->trafficMatrix()->clearSelection();
        int i;
        int j;
        if (leftPanel->trafficMatrix()->indexForChannel(asLink, i, j)) {
            leftPanel->trafficMatrix()->setCurrentCell(i, j);
        }
    } else {
        Router* asRouter = dynamic_cast<Router*>(item);
        if (asRouter) {
            //leftPanel->routingTable()->rebuild(asRouter->label(), network->routerLabels());
        }
    }
}

void MainWindow::createActions() {
    toFrontAction = new QAction(QIcon(":/images/bringtofront.png"), tr("Bring to &Front"), this);
    toFrontAction->setShortcut(tr("Ctrl+F"));
    toFrontAction->setStatusTip(tr("Bring item to front"));
    connect(toFrontAction, SIGNAL(triggered()), this, SLOT(bringToFront()));

    sendBackAction = new QAction(QIcon(":/images/sendtoback.png"), tr("Send to &Back"), this);
    sendBackAction->setShortcut(tr("Ctrl+B"));
    sendBackAction->setStatusTip(tr("Send item to back"));
    connect(sendBackAction, SIGNAL(triggered()), this, SLOT(sendToBack()));

    deleteAction = new QAction(QIcon(":/images/delete.png"), tr("&Delete"), this);
    deleteAction->setShortcut(tr("Delete"));
    deleteAction->setStatusTip(tr("Delete item from diagram"));
    connect(deleteAction, SIGNAL(triggered()), this, SLOT(deleteItem()));

    exitAction = new QAction(tr("E&xit"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Quit Scenediagram example"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
}

void MainWindow::createMenus() {
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(exitAction);

    itemMenu = menuBar()->addMenu(tr("&Item"));
    itemMenu->addAction(deleteAction);
    itemMenu->addSeparator();
    itemMenu->addAction(toFrontAction);
    itemMenu->addAction(sendBackAction);
}

void MainWindow::createToolbars() {
    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(deleteAction);
    editToolBar->addAction(toFrontAction);
    editToolBar->addAction(sendBackAction);

    QToolButton* moveRouterButton = new QToolButton;
    moveRouterButton->setCheckable(true);
    moveRouterButton->setIcon(QIcon(":/images/quad.png"));
    moveRouterButton->setToolTip("Move a router");
    QToolButton* drawLinkButton = new QToolButton;
    drawLinkButton->setCheckable(true);
    drawLinkButton->setIcon(QIcon(":/images/network-route.png"));
    drawLinkButton->setToolTip("Create a link");
    QToolButton* addRouterButton = new QToolButton();
    addRouterButton->setCheckable(true);
    addRouterButton->setIcon(QIcon(":/images/router.png"));
    addRouterButton->setToolTip("Add a router");
    addRouterButton->setChecked(true);

    pointerTypeGroup = new QButtonGroup(this);
    pointerTypeGroup->addButton(addRouterButton, static_cast<int>(Network::Mode::AddRouter));
    pointerTypeGroup->addButton(drawLinkButton, static_cast<int>(Network::Mode::CreateLink));
    pointerTypeGroup->addButton(moveRouterButton, static_cast<int>(Network::Mode::MoveItem));
    connect(pointerTypeGroup, SIGNAL(buttonClicked(int)), this, SLOT(pointerGroupClicked(int)));
    connect(network, &Network::forceModeChange, [this](Network::Mode newMode){
        for (QAbstractButton* button : this->pointerTypeGroup->buttons()) {
            if (this->pointerTypeGroup->id(button) == static_cast<int>(newMode)) {
                button->setChecked(true);
                break;
            }
        }
    });

    sceneScaleCombo = new QComboBox;
    QStringList scales;
    scales << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%");
    sceneScaleCombo->addItems(scales);
    sceneScaleCombo->setCurrentIndex(2);
    connect(sceneScaleCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(sceneScaleChanged(QString)));

    pointerToolbar = addToolBar(tr("Pointer type"));
    pointerToolbar->addWidget(addRouterButton);
    pointerToolbar->addWidget(drawLinkButton);
    pointerToolbar->addWidget(moveRouterButton);
    pointerToolbar->addWidget(sceneScaleCombo);
}

void MainWindow::analyzeNetwork() {
    Network::FullRoutingTable table = network->generateAllOptimalPaths();
    auto routerLabels = network->routerLabels();
    auto routingTable = leftPanel->routingTable();
    auto trafficMatrix = leftPanel->trafficMatrix();
    auto channelMetrics = leftPanel->channelMetrics();
    channelMetrics->setRowCount(0.5 * (routerLabels.size() - 1) * routerLabels.size());

    routingTable->rebuild(routerLabels, table);

    // in packets/bits
    double mu = 1.0 / leftPanel->packetSize();

    double lambda = 0;
    double gamma = 0;

    // Calculate gamma
    for (int i = 0; i < trafficMatrix->rowCount(); i++) {
        for (int j = 0; j < trafficMatrix->columnCount(); j++) {
            gamma += trafficMatrix->item(i, j)->text().toInt();
        }
    }

    double T = 0;

    // Perform calculations
    int row = 0;
    for (Router* source : network->routers()) {
        for (Router* dest : network->routers()) {
            Link* link = source->linkTo(dest);
            if (!link) {
                continue;
            }

            QString sourceLabel = source->label();
            QString destLabel = dest->label();

            if (sourceLabel >= destLabel) {
                continue;
            }

            int lambda_i = 0;

            QString pair = sourceLabel + destLabel;
            // Add up the traffic for all channels containing the pair
            for (int i = 0; i < routingTable->rowCount(); i++) {
                for (int j = 0; j < routingTable->columnCount(); j++) {
                    if (routingTable->item(i, j)->text().contains(pair)) {
                        lambda_i += trafficMatrix->item(i, j)->text().toInt();
                    }
                }
            }

            channelMetrics->setItem(row, 0, new QTableWidgetItem(pair));

            // Traffic through each channel (lambda i) packets/s
            lambda += lambda_i;
            channelMetrics->setItem(row, 1, new QTableWidgetItem(QString::number(lambda_i)));

            // Channel capacity ci (kbps)
            channelMetrics->setItem(row, 2, new QTableWidgetItem(QString::number(link->capacityKbps())));

            // Calculate channel service rate mu*ci (packets/s):
            // (capacity kb/s * 1000 b/kb) * (packets / bit) = packets/s
            float serviceRate = (link->capacityKbps() * 1000.0) * mu;
            channelMetrics->setItem(row, 3, new QTableWidgetItem(QString::number(serviceRate)));

            // Calculate Ti (ms):
            // 1 / ((mu * ci) - lambda i)
            float Ti = 1000 / (serviceRate - lambda_i);
            channelMetrics->setItem(row, 4, new QTableWidgetItem(QString::number(Ti)));

            T += (lambda_i * Ti) / lambda;

            row++;
        }
    }

    double n = 0;

    n = lambda / gamma;
    T *= n;

    leftPanel->T()->setText(QString::number(T));
    leftPanel->n()->setText(QString::number(n));
    leftPanel->lambda()->setText(QString::number(lambda));
    leftPanel->gamma()->setText(QString::number(gamma));
}
