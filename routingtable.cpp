#include "routingtable.h"

#include <cassert>
#include <QDebug>
#include <QSet>

RoutingTable::RoutingTable(QWidget* parent) : ChannelAddressableTable(parent) { }

void RoutingTable::rebuild(QStringList routers, Network::FullRoutingTable table) {
    assert(std::is_sorted(routers.begin(), routers.end()));

    clearAll();

    if (routers.empty()) {
        return;
    }

    // Create column & row headers
    std::reverse_copy(std::next(routers.begin()), routers.end(), std::back_inserter(columnLabels));
    std::copy(routers.begin(), std::prev(routers.end()), std::back_inserter(rowLabels));
    reconstruct();

    // Black out uneditable cells
    int routerCount = routers.size();
    for (int i = 0; i < routerCount - 1; i++) {
        for (int j = routerCount - i - 1; j < routerCount - 1; j++) {
            disableCell(i, j);
        }
    }

    // Load table
    for (QPair<Router*, Router*> key : table.keys()) {
        auto path = table[key];
        QString display = "";
        for (Router* router : path) {
            display += router->label();
        }

        auto cell = cellForChannel(key.first->label(), key.second->label(), false);
        if (cell) {
            cell->setFlags(!Qt::ItemIsEditable);
            cell->setText(display);
        }
    }
}
