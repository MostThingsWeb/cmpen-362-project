#include "trafficmatrix.h"
#include "utility.h"

#include <algorithm>
#include <cassert>

#include <QDebug>

TrafficMatrix::TrafficMatrix(QWidget *parent) : ChannelAddressableTable(parent) { }

void TrafficMatrix::rebuild(const QStringList& routers) {
    assert(std::is_sorted(routers.begin(), routers.end()));

    if (routers.empty()) {
        clearAll();
        return;
    }

    Snapshot snapshot = capture();
    clearAll();

    // Create column & row headers
    std::reverse_copy(std::next(routers.begin()), routers.end(), std::back_inserter(columnLabels));
    std::copy(routers.begin(), std::prev(routers.end()), std::back_inserter(rowLabels));
    reconstruct();

    // Black out uneditable cells
    int routerCount = routers.size();
    for (int i = 0; i < routerCount - 1; i++) {
        for (int j = routerCount - i - 1; j < routerCount - 1; j++) {
            disableCell(i, j);
        }
    }

    restore(snapshot);

    // Randomize empty cells
    for (int i = 0; i < routerCount; i++) {
        for (int j = 0; j < routerCount; j++) {
            if (item(i, j) && item(i, j)->text().length() == 0 && !isCellDisabled(i, j)) {
                item(i, j)->setText(QString::number(Utility::randUniform(1, 20)));
            }
        }
    }
}
