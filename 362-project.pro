#-------------------------------------------------
#
# Project created by QtCreator 2015-11-06T13:40:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 362-project
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    link.cpp \
    router.cpp \
    utility.cpp \
    leftpanel.cpp \
    trafficmatrix.cpp \
    linklabel.cpp \
    routingtable.cpp \
    channeladdressabletable.cpp \
    network.cpp \
    channelmetricstable.cpp

HEADERS  += mainwindow.h \
    link.h \
    router.h \
    utility.h \
    leftpanel.h \
    trafficmatrix.h \
    linklabel.h \
    routingtable.h \
    channeladdressabletable.h \
    network.h \
    channelmetricstable.h

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    background1.png
