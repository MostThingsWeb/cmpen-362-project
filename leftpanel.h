#ifndef LEFTPANEL_H
#define LEFTPANEL_H

#include <QObject>
#include <QStackedWidget>
#include <QSplitter>
#include <QLineEdit>
#include <QComboBox>
#include <QSpinBox>
#include <QPushButton>

#include "trafficmatrix.h"
#include "channelmetricstable.h"
#include "routingtable.h"

class LeftPanel : public QWidget {
    Q_OBJECT
public:
    LeftPanel(QWidget* parent = 0);

    TrafficMatrix* trafficMatrix();
    ChannelMetricsTable* channelMetrics();
    RoutingTable* routingTable();
    QPushButton* analyzeButton();
    int packetSize() const;

    QLineEdit* n() { return _n; }
    QLineEdit* T() { return _T; }
    QLineEdit* lambda() { return _lambda; }
    QLineEdit* gamma() { return _gamma; }


private:
    void createNetworkAnalyzerGroup();
    void createParametersGroup();
    void createRoutingTableGroup();

    TrafficMatrix* _trafficMatrix = nullptr;
    RoutingTable* _routingTable = nullptr;
    ChannelMetricsTable* _channelMetrics = nullptr;

    QSplitter* _splitter = nullptr;
    QSpinBox* _packetSize = nullptr;
    QPushButton* _analyzeButton = nullptr;

    QLineEdit* _n = nullptr;
    QLineEdit* _T = nullptr;
    QLineEdit* _lambda = nullptr;
    QLineEdit* _gamma = nullptr;
};

#endif // LEFTPANEL_H
