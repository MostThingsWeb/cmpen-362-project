#include "utility.h"
#include <cmath>


QString Utility::alphaLabel(int value) {
    QString result;
    while (--value >= 0) {
        result = (char)('A' + value % 26) + result;
        value /= 26;
    }
    return result;
}

int Utility::randUniform(int min, int max) {
    // Seed with a real random value, if available
    std::random_device rd;

    std::default_random_engine eng(rd());
    std::uniform_int_distribution<int> uniform_dist(min, max);
    return uniform_dist(eng);
}

int Utility::randNormal(int mean, int stdev) {
    // Seed with a real random value, if available
    std::random_device rd;

    std::default_random_engine eng(rd());
    std::normal_distribution<float> normal_dist(mean, stdev);
    return std::max<int>(1, trunc(normal_dist(eng)));
}
