// Graphical portions of this project are based off of Qt graphics scene example (originally copyright below).
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "router.h"
#include "link.h"
#include "utility.h"

#include <QGraphicsScene>
#include <QGraphicsSceneContextMenuEvent>
#include <QMenu>
#include <QPainter>
#include <QDebug>

#include <algorithm>
#include <iostream>
#include <set>
#include <numeric>

Router::Router(QMenu* contextMenu,
               QGraphicsItem* parent)
    : QGraphicsPolygonItem(parent) {
    myContextMenu = contextMenu;

    QPainterPath path;
    path.moveTo(50, 50);
    path.addEllipse(-50, -50, 100, 100);
    myPolygon = path.toFillPolygon();
    setPolygon(myPolygon);

    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

void Router::removelink(Link* link) {
    int index = _links.indexOf(link);

    if (index != -1) {
        _links.removeAt(index);
    }
}

void Router::removelinks() {
    foreach(Link * link, _links) {
        link->source()->removelink(link);
        link->dest()->removelink(link);
        scene()->removeItem(link);
        delete link;
    }
}

QPolygonF Router::polygon() const {
    return myPolygon;
}

void Router::addlink(Link* link) {
    _links.append(link);
}

int Router::type() const {
    return Type;
}

int Router::number() const {
    return _index;
}

Link *Router::linkTo(Router *other) {
    for (Link* link : _links) {
        if (link->source() == other || link->dest() == other) {
            return link;
        }
    }

    return nullptr;
}

QList<Router *> Router::neighbors() {
    QList<Router*> ret;
    for (Link* link : _links) {
        ret.append(link->source() == this ? link->dest() : link->source());
    }
    return ret;
}

const QList<Link *> &Router::links() {
    return _links;
}

void Router::computeLabel() {
    if (this->scene()) {
        auto items = this->scene()->items();

        // Construct sorted list of used indexes
        std::vector<int> usedIndexes;
        std::for_each(items.begin(), items.end(), [&](QGraphicsItem* item) {
            Router* asRouter = (item && this != item) ? dynamic_cast<Router*>(item) : nullptr;
            if (asRouter) {
                usedIndexes.push_back(asRouter->number());
            }
        });
        std::sort(usedIndexes.begin(), usedIndexes.end());

        // Locate hole in series of indexes, or if no hole exists, add 1 to greatest index
        std::vector<int> expectedIndexes(usedIndexes.size());
        // Create range of numbers, [1, usedIndexes.size()]
        std::iota(std::begin(expectedIndexes), std::end(expectedIndexes), 1);

        auto mismatch = std::mismatch(expectedIndexes.begin(), expectedIndexes.end(), usedIndexes.begin());
        auto& nextIndex = mismatch.first;
        if (nextIndex == expectedIndexes.end()) {
            // No mismatch, so index is number of routers
            _index = usedIndexes.size() + 1;
        } else {
            _index = *nextIndex;
        }

        _label = Utility::alphaLabel(_index);
    }
}

QString Router::label() const {
    return _label;
}

void Router::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    QGraphicsPolygonItem::paint(painter, option, widget);

    painter->setFont(QFont("Sans Serif", 12));
    painter->drawText(myPolygon.boundingRect(), Qt::AlignCenter, _label);
}

void Router::contextMenuEvent(QGraphicsSceneContextMenuEvent* event) {
    scene()->clearSelection();
    setSelected(true);
    myContextMenu->exec(event->screenPos());
}

QVariant Router::itemChange(GraphicsItemChange change, const QVariant& value) {
    if (change == QGraphicsItem::ItemPositionChange) {
        foreach(Link * link, _links) {
            link->updatePosition();
        }
    }

    return value;
}
