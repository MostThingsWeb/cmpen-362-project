// Graphical portions of this project are based off of Qt graphics scene example (originally copyright below).
/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "router.h"
#include "leftpanel.h"

#include <QMainWindow>
#include <QHBoxLayout>
#include <QToolBox>
#include <QGraphicsScene>
#include <QAbstractButton>
#include <QString>
#include <QComboBox>
#include <QFontComboBox>
#include <QToolButton>
#include <QList>


class Network;

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow();

private slots:
    void deleteItem();
    void pointerGroupClicked(int id);
    void bringToFront();
    void sendToBack();
    void itemInserted(Router* item);
    void sceneScaleChanged(const QString& scale);

private:
    void createToolBox();
    void createActions();
    void createMenus();
    void createToolbars();

    void analyzeNetwork();

    void itemsSelected(QGraphicsItem* item);

    QMenu* createColorMenu(const char* slot, QColor defaultColor);
    QIcon createColorToolButtonIcon(const QString& image, QColor color);
    QIcon createColorIcon(QColor color);

    Network* network = nullptr;
    QGraphicsView* view = nullptr;

    LeftPanel* leftPanel = nullptr;

    QAction* exitAction = nullptr;
    QAction* addAction = nullptr;
    QAction* deleteAction = nullptr;
    QAction* toFrontAction = nullptr;
    QAction* sendBackAction = nullptr;

    QMenu* fileMenu = nullptr;
    QMenu* itemMenu = nullptr;

    QToolBar* textToolBar = nullptr;
    QToolBar* editToolBar = nullptr;
    QToolBar* pointerToolbar = nullptr;

    QComboBox* sceneScaleCombo = nullptr;

    QButtonGroup* pointerTypeGroup = nullptr;
    QAction* lineAction = nullptr;
};

#endif // MAINWINDOW_H
