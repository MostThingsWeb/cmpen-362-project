#ifndef ROUTINGTABLE_H
#define ROUTINGTABLE_H

#include <QString>

#include "channeladdressabletable.h"
#include "link.h"
#include "network.h"

class RoutingTable : public ChannelAddressableTable
{
    Q_OBJECT
public:
    explicit RoutingTable(QWidget *parent = 0);

    void rebuild(QStringList allRouters, Network::FullRoutingTable table);
};

#endif // ROUTINGTABLE_H
