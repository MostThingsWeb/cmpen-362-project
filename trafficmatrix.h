#ifndef TRAFFICMATRIX_H
#define TRAFFICMATRIX_H

#include <QString>

#include "channeladdressabletable.h"
#include "link.h"

class TrafficMatrix : public ChannelAddressableTable
{
    Q_OBJECT
public:
    explicit TrafficMatrix(QWidget *parent = 0);

    void rebuild(const QStringList& routers);

};

#endif // TRAFFICMATRIX_H
