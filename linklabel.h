#ifndef LINKCOSTLABEL_H
#define LINKCOSTLABEL_H

#include <QGraphicsTextItem>

#include "linklabel.h"

class LinkLabel;

#include "link.h"

class LinkLabel : public QGraphicsTextItem
{
    Q_OBJECT
public:
    explicit LinkLabel(const QString &text, Link *parent);

    void focusOutEvent(QFocusEvent *event) override;
    void focusInEvent(QFocusEvent *event) override;

    void updatePosition();

signals:
    void linkCapacityChanged(int capacityKbps);

public slots:

};

#endif // LINKCOSTLABEL_H
