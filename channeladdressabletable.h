#ifndef CHANNELADDRESSABLETABLE_H
#define CHANNELADDRESSABLETABLE_H

#include <QTableWidget>
#include <QMap>
#include <QPair>

#include "link.h"

class ChannelAddressableTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit ChannelAddressableTable(QWidget *parent = 0);

    virtual QTableWidgetItem* cellForChannel(QString endpoint1, QString endpoint2, bool allowFlip = true);
    virtual bool indexForChannel(Link* link, int& source, int& dest, bool allowFlip = true);
    virtual bool indexForChannel(QString sourceLabel, QString destLabel, int& source, int& dest, bool allowFlip = true);

    virtual void disableCell(int row, int column);
    virtual bool isCellDisabled(int row, int column);

    virtual void reconstruct();

    void clearAll();

protected:
    typedef QMap<QPair<QString, QString>, QString> Snapshot;

    QStringList rowLabels;
    QStringList columnLabels;

    virtual Snapshot capture();
    virtual void restore(Snapshot);

};

#endif // CHANNELADDRESSABLETABLE_H
