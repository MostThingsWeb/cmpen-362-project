#include "channelmetricstable.h"

ChannelMetricsTable::ChannelMetricsTable(QWidget *parent) : QTableWidget(parent) {
    setColumnCount(5);
    setHorizontalHeaderLabels({"Line", "λi (packets/s)", "ci (kbps)", "μci (packets/s)", "Ti (ms)"});
}
