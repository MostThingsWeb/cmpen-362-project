#include "linklabel.h"
#include "link.h"

#include <QFont>

LinkLabel::LinkLabel(const QString &text, Link *parent) : QGraphicsTextItem(text, parent) {
    setTextInteractionFlags(Qt::TextEditable);
    setFont(QFont("Courier New", 12));

    if (parent->source() && parent->dest()) {
        setToolTip(QString("Capacity (kbps) of channel %0 => %1").arg(parent->source()->label()).arg(parent->dest()->label()));
    }
}

void LinkLabel::focusOutEvent(QFocusEvent *event) {
    QIntValidator validator(0, INT_MAX);

    int pos = 0;
    int value;
    QString rawValue = this->toPlainText();

    if (validator.validate(rawValue, pos) == QValidator::State::Acceptable) {
        // Normalize number
        value = rawValue.toInt();
        this->setPlainText(QString::number(value));
    } else {
        this->setPlainText("0 kbps");
        value = 0;
    }

    QGraphicsTextItem::focusOutEvent(event);
    emit linkCapacityChanged(value);
}

void LinkLabel::focusInEvent(QFocusEvent *event) {
    scene()->clearSelection();
    parentItem()->setSelected(true);

    QGraphicsTextItem::focusInEvent(event);
}

void LinkLabel::updatePosition() {
    Link* link = dynamic_cast<Link*>(parentItem());
    if (link) {
        QLineF line = link->line();
        QTransform labelTransform;
        // Rotate label to match line (correcting the flip if start point is farther to the right than end point)
        labelTransform.rotate(-line.angle() + (line.p1().x() > line.p2().x() ? 180 : 0));

        // Adjust the label's origin to be above the horizontal center
        labelTransform.translate(-(boundingRect().width() / 2), -40);
        setTransform(labelTransform);

        // Move label origin to be center of line
        setPos(line.pointAt(0.5));
        setZValue(this->zValue() + 1);
    }
}
