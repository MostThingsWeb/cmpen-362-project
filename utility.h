#ifndef UTILITY_H
#define UTILITY_H

#include <QString>

class Utility {
public:
    // http://codereview.stackexchange.com/a/44094
    static QString alphaLabel(int value);

    static int randUniform(int min, int max);

    static int randNormal(int mean, int stdev);
};

#endif // UTILITY_H
