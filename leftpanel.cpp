#include "leftpanel.h"
#include "utility.h"
#include "link.h"

#include <cmath>

#include <QVBoxLayout>
#include <QGroupBox>
#include <QSplitter>
#include <QLineEdit>
#include <QGridLayout>
#include <QLabel>

LeftPanel::LeftPanel(QWidget *parent) : QWidget(parent) {
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    _splitter = new QSplitter(Qt::Vertical);
    _splitter->setContentsMargins(10, 10, 5, 10);

    createParametersGroup();
    createNetworkAnalyzerGroup();
    createRoutingTableGroup();

    QVBoxLayout* layout = new QVBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(_splitter, 1);

    setMinimumWidth(325);

    this->setLayout(layout);
}

TrafficMatrix *LeftPanel::trafficMatrix() {
    return _trafficMatrix;
}

ChannelMetricsTable *LeftPanel::channelMetrics() {
    return _channelMetrics;
}

RoutingTable *LeftPanel::routingTable() {
    return _routingTable;
}

QPushButton *LeftPanel::analyzeButton() {
    return _analyzeButton;
}

int LeftPanel::packetSize() const {
    return _packetSize->value();
}

void LeftPanel::createNetworkAnalyzerGroup() {
    _channelMetrics = new ChannelMetricsTable();

    _analyzeButton = new QPushButton("Analyze network");

    QGridLayout* grid = new QGridLayout();
    grid->setAlignment(Qt::AlignTop);
    grid->setColumnStretch(1, 1);

    _n = new QLineEdit();
    _n->setEnabled(false);
    _T = new QLineEdit();
    _T->setEnabled(false);
    _lambda = new QLineEdit();
    _lambda->setEnabled(false);
    _gamma = new QLineEdit();
    _gamma->setEnabled(false);

    grid->addWidget(new QLabel("n: "), 0, 0);
    grid->addWidget(_n, 0, 1);

    grid->addWidget(new QLabel("T: "), 1, 0);
    grid->addWidget(_T, 1, 1);
    grid->addWidget(new QLabel(" ms"), 1, 2);

    grid->addWidget(new QLabel("λ: "), 2, 0);
    grid->addWidget(_lambda, 2, 1);
    grid->addWidget(new QLabel(" packets/s"), 2, 2);

    grid->addWidget(new QLabel("γ: "), 3, 0);
    grid->addWidget(_gamma, 3, 1);
    grid->addWidget(new QLabel(" packets/s"), 3, 2);


    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(_analyzeButton);
    layout->addLayout(grid);
    layout->addWidget(_channelMetrics);

    QGroupBox* group = new QGroupBox("Network analysis");
    group->setContentsMargins(5, 5, 5, 5);
    group->setLayout(layout);

    _splitter->addWidget(group);
}

void LeftPanel::createParametersGroup() {
    QGridLayout* layout = new QGridLayout();
    layout->setAlignment(Qt::AlignTop);
    layout->setColumnStretch(1, 1);

    _packetSize = new QSpinBox();
    _packetSize->setMinimum(1);
    _packetSize->setMaximum(INT_MAX);
    _packetSize->setValue(Utility::randUniform(256, 2048));
    _packetSize->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    layout->addWidget(new QLabel("Mean packet size:"), 0, 0, Qt::AlignLeft);
    layout->addWidget(_packetSize, 0, 1);
    layout->addWidget(new QLabel("bits/packet"), 0, 2, Qt::AlignLeft);

    _trafficMatrix = new TrafficMatrix();
    layout->addWidget(new QLabel("Channel traffic: "), 1, 0, 1, 3);
    layout->addWidget(_trafficMatrix, 2, 0, 1, 3);

    QGroupBox* group = new QGroupBox("Network parameters");
    group->setContentsMargins(5, 5, 5, 5);
    group->setLayout(layout);

    _splitter->addWidget(group);
}

void LeftPanel::createRoutingTableGroup() {
    _routingTable = new RoutingTable();

    QVBoxLayout* layout = new QVBoxLayout();
    layout->addWidget(_routingTable);

    QGroupBox* group = new QGroupBox("Routing table");
    group->setContentsMargins(5, 5, 5, 5);
    group->setLayout(layout);

    _splitter->addWidget(group);
}
