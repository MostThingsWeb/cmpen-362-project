#include <QDebug>
#include "channeladdressabletable.h"

ChannelAddressableTable::ChannelAddressableTable(QWidget *parent) : QTableWidget(parent) { }

ChannelAddressableTable::Snapshot ChannelAddressableTable::capture() {
    // Capture current table contents
    Snapshot oldData;

    for (auto fromIt = rowLabels.begin(); fromIt != rowLabels.end(); ++fromIt) {
        for (auto toIt = columnLabels.begin(); toIt != columnLabels.end(); ++toIt) {
            int i = std::distance(rowLabels.begin(), fromIt);
            int j = std::distance(columnLabels.begin(), toIt);

            auto itemPtr = item(i, j);
            oldData[QPair<QString, QString>(*fromIt, *toIt)] = itemPtr ? itemPtr->text() : "";
        }
    }

    return oldData;
}

void ChannelAddressableTable::restore(ChannelAddressableTable::Snapshot snapshot) {
    for (auto it = snapshot.begin(); it != snapshot.end(); ++it) {
        QTableWidgetItem* cell = cellForChannel(it.key().first, it.key().second);
        if (cell) {
            cell->setText(it.value());
        }
    }
}


QTableWidgetItem *ChannelAddressableTable::cellForChannel(QString source, QString dest, bool allowFlip) {
    int i;
    int j;
    if (indexForChannel(source, dest, i, j, allowFlip)) {
        return item(i, j);
    }

    return nullptr;
}

bool ChannelAddressableTable::indexForChannel(Link *link, int &source, int &dest, bool allowFlip) {
    return indexForChannel(link->source()->label(), link->dest()->label(), source, dest, allowFlip);
}

bool ChannelAddressableTable::indexForChannel(QString sourceLabel, QString destLabel, int &source, int &dest, bool allowFlip) {
    int i = rowLabels.indexOf(sourceLabel);
    int j = columnLabels.indexOf(destLabel);

    if (i < 0 || j < 0 || isCellDisabled(i, j)) {
        if (allowFlip) {
            // Try with source & dest
            i = rowLabels.indexOf(destLabel);
            j = columnLabels.indexOf(sourceLabel);

            if (i < 0 || j < 0) {
                return false;
            }
        } else {
            return false;
        }
    }

    source = i;
    dest = j;
    return true;
}

void ChannelAddressableTable::disableCell(int row, int column) {
    item(row, column)->setFlags(!Qt::ItemIsEditable);
    item(row, column)->setBackgroundColor(Qt::black);
}

bool ChannelAddressableTable::isCellDisabled(int row, int column) {
    return !(item(row, column)->flags() & Qt::ItemIsEditable);
}

void ChannelAddressableTable::reconstruct() {
    int rowCount = rowLabels.size();
    int colCount = columnLabels.size();

    setRowCount(rowCount);
    setColumnCount(colCount);

    setHorizontalHeaderLabels(columnLabels);
    setVerticalHeaderLabels(rowLabels);

    // Initialize cells, and resize columns
    setItem(0, 0, new QTableWidgetItem("10000"));
    resizeColumnToContents(0);
    int width = columnWidth(0);
    for (int j = 0; j < colCount; j++) {
        if (j > 0) {
            setColumnWidth(j, width);
        }
        for (int i = 0; i < rowCount; i++) {
            setItem(i, j, new QTableWidgetItem(""));
        }
    }
    setItem(0, 0, new QTableWidgetItem(""));
}

void ChannelAddressableTable::clearAll() {
    clear();
    rowLabels.clear();
    columnLabels.clear();
}
